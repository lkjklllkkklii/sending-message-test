import { Body, Controller, Post } from '@nestjs/common';
import { CreateMessageDto } from './dto/create-message.dto';
import { MessageService } from './message.service';
import { ResponseModel } from './interface/message.model';

@Controller('message')
export class MessageController {

    constructor(private readonly messageService: MessageService) {}

    @Post()
    sendMessage(@Body() createMessageDto: CreateMessageDto): ResponseModel {
        return this.messageService.create(createMessageDto);
    }
}
