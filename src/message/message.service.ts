import { Injectable } from '@nestjs/common';
import { CreateMessageDto } from './dto/create-message.dto';
import { ResponseModel } from './interface/message.model';

@Injectable()
export class MessageService {
    create(createMessageDto: CreateMessageDto): ResponseModel {
        const { conversation_id, message } = createMessageDto;        
        const responses: string[] = ['Welcome to Station Five.', 'Thank you, see you around.', 'Sorry, I don’t understand.'];
        let response: string = '';
        if(['Hi', 'Hello'].some(f => message.includes(f))) response = responses[0];
        else if(['Goodbye', 'bye'].some(f => message.includes(f))) response = responses[1];
        else response = responses[2];
        return { response_id: conversation_id, response };
    }
}
