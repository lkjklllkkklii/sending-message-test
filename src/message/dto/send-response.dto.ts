export class SendResponseDto {
    response_id: string;
    response: string;
}