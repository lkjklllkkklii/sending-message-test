export class CreateMessageDto {
    conversation_id: string;
    message: string;
}