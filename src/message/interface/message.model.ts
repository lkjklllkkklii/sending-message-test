export interface MessageModel {
    conversation_id: string,
    message: string
}

export interface ResponseModel {
    response_id: string;
    response: string;
}